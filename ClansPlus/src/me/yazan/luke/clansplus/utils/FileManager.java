package me.yazan.luke.clansplus.utils;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public class FileManager {
	
	private static FileManager instance = new FileManager();
	
	private File clanData;
	private File playerData;
	
	private FileConfiguration   clanConf;
	private FileConfiguration playerConf;
	
	public static FileManager getInstance()
	{
		return instance;
	}
	public void trySetup(Plugin p)
	{
		this.clanData   = new File("plugins" + File.separator + p.getDataFolder().getName() + File.separator +   "clan.yml");
		this.playerData = new File("plugins" + File.separator + p.getDataFolder().getName() + File.separator + "player.yml");
		
		if(!(p.getDataFolder().exists()))
		{
			p.getDataFolder().mkdir();
		}
		
		if((!clanData.exists()) && (!playerData.exists()))
		{
			try { clanData.createNewFile(); } catch(IOException ioe) { ioe.printStackTrace(); }
			try { playerData.createNewFile(); } catch(IOException ioe) { ioe.printStackTrace(); }
		}

		clanConf   = YamlConfiguration.loadConfiguration(  clanData);
		playerConf = YamlConfiguration.loadConfiguration(playerData);
	}
	
	public FileConfiguration getClanFile()
	{
		return clanConf;
	}
	
	public FileConfiguration getPlayerFile()
	{
		return playerConf;
	}
	
	public void saveFiles()
	{
		try { clanConf.save(clanData); } catch(IOException ioe) { ioe.printStackTrace(); }
		try { playerConf.save(playerData); } catch(IOException ioe) { ioe.printStackTrace(); }
	}
	
}
