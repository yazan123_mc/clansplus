package me.yazan.luke.clansplus.utils;


import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.plugin.Plugin;

public class Settings {
	

	private Plugin p;
	private static Settings instance = new Settings();
	
	public static Settings get()
	{
		return instance;
	}
	
	public void setupSettings(Plugin p)
	{
		this.p = p;
	}
	
	public Plugin getPlugin()
	{
		return p;
	}
	
    public String serializeLoc(Location l)
    {
        return 
        	   l.getWorld().getName()+","
              +l.getBlockX()+","
              +l.getBlockY()+","
              +l.getBlockZ()+","
              +l.getYaw()+","
              +l.getPitch();
    }
    
    public Location deserializeLoc(String l)
    {
        String[] l_a = l.split(",");
        
        return new Location(Bukkit.getWorld(l_a[0]),
        		           Integer.parseInt(l_a[1]),
        		           Integer.parseInt(l_a[2]),
        		           Integer.parseInt(l_a[3]),
        		           Float.parseFloat(l_a[4]),
        		           Float.parseFloat(l_a[5]));
    }	
}
