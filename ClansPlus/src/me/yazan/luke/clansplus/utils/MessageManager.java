package me.yazan.luke.clansplus.utils;

import org.bukkit.ChatColor;

public class MessageManager {
	
	private static MessageManager mm = new MessageManager();
	
	public String prefix = ChatColor.GRAY + "" +ChatColor.YELLOW + "[" + ChatColor.GRAY + "ClansPlus" + ChatColor.YELLOW + "] ";
	
	public static MessageManager get()
	{
		return mm;
	}
	
	public String info(String msg)
	{
		return prefix + ChatColor.GRAY + msg;
	}
	
	public String success(String msg)
	{
		return prefix + ChatColor.GREEN + msg;
	}
	
	public String fail(String msg)
	{
		return prefix + ChatColor.RED + msg;
	}
		
}
