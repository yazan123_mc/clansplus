package me.yazan.luke.clansplus.commands;

import me.yazan.luke.clansplus.players.ClansPlayer;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class StatsCommand extends SubCommand{

	@Override
	public void execute(String[] args, String permission, Player p) 
	{
		if(!(p.hasPermission(permission) || p.isOp()))
		{
			noPermissionMessage(p);
			return;
			
		} else {
			
			ClansPlayer cp = new ClansPlayer(p);
		
		// To finish
		p.sendMessage(ChatColor.YELLOW + "Personal stats");
        p.sendMessage(cp.getKills() + "Kills");
        p.sendMessage(cp.getDeaths() + "Deaths");
        p.sendMessage(cp.getKDR() + "KD Ratio");
		
	}
	}

}
