package me.yazan.luke.clansplus.commands;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public abstract class SubCommand {
	
	public abstract void execute(String args[], String permission, Player p);
	
	public void noPermissionMessage(Player p)
	{
		p.sendMessage(ChatColor.RED + "No permission.");
	}
}
