package me.yazan.luke.clansplus.commands;

import me.yazan.luke.clansplus.utils.MessageManager;
import me.yazan.luke.clansplus.utils.Settings;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CLPCommand implements CommandExecutor
{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lbl,
			String[] args) 
	{
		if(sender instanceof Player)
		{
			Player player = (Player) sender;
			
			if(cmd.getName().equalsIgnoreCase("clansplus"))
			{
				if(args.length == 0)
				{
					//send info
					
					player.sendMessage(MessageManager.get().prefix + ":-");
					player.sendMessage(ChatColor.GRAY + "Version: " + ChatColor.YELLOW + Settings.get().getPlugin().getDescription().getVersion());
					player.sendMessage(ChatColor.GRAY + "Developed by: "+ChatColor.YELLOW+" Yazan123 & Luke199");
					
					return true;
				}
				switch(args[0].toUpperCase())
				{
				case "HELP":
					new HelpCommand().execute(args, "clansplus.cmd.help", player);
					break;
					
				case "STATS":
					new StatsCommand().execute(args, "clansplus.cmd.personalsats", player);
					
				default:
					player.sendMessage(ChatColor.RED + "Invalid sub command.");
					break;
				}
			}
		} else return true;
		
		return false;
	}
	
	

}
