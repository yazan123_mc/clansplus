package me.yazan.luke.clansplus;

import java.io.File;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import me.yazan.luke.clansplus.clans.Clan;
import me.yazan.luke.clansplus.clans.ClanManager;
import me.yazan.luke.clansplus.commands.CLPCommand;
import me.yazan.luke.clansplus.listeners.FriendlyFireDamage;
import me.yazan.luke.clansplus.listeners.JoinGame;
import me.yazan.luke.clansplus.listeners.KillOthers;
import me.yazan.luke.clansplus.listeners.LeaveGame;
import me.yazan.luke.clansplus.utils.FileManager;
import me.yazan.luke.clansplus.utils.Settings;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class ClansPlus extends JavaPlugin{

	public Logger              logs;
	public FileConfiguration config;
	
	@Override
	public void onEnable()
	{
		logs = this.getLogger();
		logs.info("Enabled.");
		config = getConfig();
		

		
		File configFile = new File(this.getDataFolder() + "/config.yml");
		if(!configFile.exists())
		{
			
			//Tweaks
			
			//config.addDefault("",<>);
			config.options().copyDefaults(true);
			saveConfig();
		}

		Settings.get().setupSettings(this);
		FileManager.getInstance().trySetup(this);
		
	    loadClans();
		
		Bukkit.getPluginManager().registerEvents(new FriendlyFireDamage(), this);
		Bukkit.getPluginManager().registerEvents(new         KillOthers(), this);
		Bukkit.getPluginManager().registerEvents(new           JoinGame(), this);
		Bukkit.getPluginManager().registerEvents(new          LeaveGame(), this);
		
		this.getCommand("clansplus").setExecutor(new CLPCommand());
		
			
		
	}
	
	@Override
	public void onDisable()
	{
		this.logs.info("Disabled.");
	}
	
	public void loadClans()
	{
		if(FileManager.getInstance().getClanFile().getConfigurationSection("Clans") == null)
		{
			logs.info("Loaded 0 clans.");
			return;
		}
		for(String ks : FileManager.getInstance().getClanFile().getConfigurationSection("Clans").getKeys(false))
		{
			
			UUID leader = UUID.fromString(FileManager.getInstance().getClanFile().getString("Clans." + ks + ".leader"));
			String tag  =                     FileManager.getInstance().getClanFile().getString("Clans." + ks + ".tag");
			
			Clan c = new Clan(leader, ks, tag);
			
			c.setExp(FileManager.getInstance().getClanFile().getLong("Clans." + c.getName() + ".xp"));
			c.setExpNeeded(FileManager.getInstance().getClanFile().getLong("Clans." + c.getName() + ".xp-next"));
			c.setLevel(FileManager.getInstance().getClanFile().getInt("Clans." + c.getName() + ".level"));
			
			if(FileManager.getInstance().getClanFile().getString("Clans." + ks + ".hq-loc") != null)
			{
				c.setHQLocation(Settings.get().deserializeLoc
						(FileManager.getInstance().getClanFile().getString("Clans." + ks + ".hq-loc")));
			}
			
			if(FileManager.getInstance().getClanFile().getStringList("Clans." + c.getName() + ".recruits") != null)
			{
				List<String> recruits_list = FileManager.getInstance().getClanFile().getStringList("Clans." + c.getName() + ".recruits");
				
				for(String r : recruits_list)
				{
					c.getRecruits().add(UUID.fromString(r));
				}
			}
			
			if(FileManager.getInstance().getClanFile().getStringList("Clans." + c.getName() + ".members") != null)
			{
				List<String> members_list = FileManager.getInstance().getClanFile().getStringList("Clans." + c.getName() + ".members");
				
				for(String r : members_list)
				{
					c.getMembers().add(UUID.fromString(r));
				}
			}

			if(FileManager.getInstance().getClanFile().getStringList("Clans." + c.getName() + ".sergeants") != null)
			{
				List<String> members_list = FileManager.getInstance().getClanFile().getStringList("Clans." + c.getName() + ".sergeants");
				
				for(String r : members_list)
				{
					c.getSergeants().add(UUID.fromString(r));
				}
			}
			
			if(FileManager.getInstance().getClanFile().getString("Clans." + c.getName() + ".captain") != null)
			{
				c.clan_captain = UUID.fromString(FileManager.getInstance().getClanFile().getString("Clans." + c.getName() + ".captain"));
			}
			
			ClanManager.get().clans_list.add(c);
			Settings.get().getPlugin().getLogger().info("Loaded " + ClanManager.get().clans_list.size() + " clans.");
		}
	}

}
