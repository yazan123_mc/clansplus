package me.yazan.luke.clansplus.clans;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import me.yazan.luke.clansplus.utils.FileManager;
import me.yazan.luke.clansplus.utils.MessageManager;
import me.yazan.luke.clansplus.utils.Settings;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class Clan {
	
	private ArrayList<UUID> clan_members   = new ArrayList<UUID>();
	private ArrayList<UUID> clan_recruits  = new ArrayList<UUID>();
	private ArrayList<UUID> clan_sergeants = new ArrayList<UUID>();
	
	private String          name;
	private String           tag;
	
	private int            level;
	private int      total_kills;
	private int     total_deaths;
	
	private float            kdr;
	
	private long         clan_xp;
	private long       xp_needed;
	

	private UUID     clan_leader;
	
	public  UUID    clan_captain;
	
	private Location clan_hq_loc;
	
	/**
	 * 
	 * @param leader the player who created the clan
	 * @param name the clan name
	 * @param tag 4 character string to use in chat
	 */
	public Clan(UUID leader, String name, String tag)
	{
		this.clan_leader = leader;
		
		this.name = name;
		this.tag = tag;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getTag()
	{
		return tag;
	}
	
	public ArrayList<UUID> getMembers()
	{
		return clan_members;
	}
	
	public ArrayList<String> getMembersToString()
	{
		ArrayList<String> l = new ArrayList<String>();
		
		for(UUID uuid : this.getMembers())
		{
			l.add(uuid.toString());
		}
		return l;
	}
	
	public ArrayList<UUID> getRecruits()
	{
		return clan_recruits;
	}
	
	public ArrayList<String> getRecruitsToString()
	{
		ArrayList<String> l = new ArrayList<String>();
		
		for(UUID uuid : this.getRecruits())
		{
			l.add(uuid.toString());
		}
		return l;
	}
	
	public ArrayList<UUID> getSergeants()
	{
		return clan_sergeants;
	}
	
	public ArrayList<String> getSergeantsToString()
	{
		ArrayList<String> l = new ArrayList<String>();
		
		for(UUID uuid : this.getSergeants())
		{
			l.add(uuid.toString());
		}
		return l;
	}

	public UUID getLeader()
	{
		return clan_leader;
	}
	
	public String getLeaderToString()
	{
		return clan_leader.toString();
	}
	
	public UUID getCaptain()
	{
		return clan_captain;
	}
	
	public String getCaptainToString()
	{
		return clan_captain.toString();
	}
	
	public int getLevel()
	{
		return level;
	}
	
	public long getExp()
	{
		return clan_xp;
	}
	
	public long getExpNeeded()
	{
		return xp_needed;
	}
	
	public int getTotalKills()
	{
		return total_kills;
	}
	
	public int getTotalDeaths()
	{
		return total_deaths;
	}
	
	public Location getHQLocation()
	{
		return clan_hq_loc;
	}
	
	public float getKDR()
	{
		if(this.getTotalKills() >= this.getTotalDeaths())
		{
			this.kdr = total_deaths/ total_kills;
			return kdr;
		}
		if(this.getTotalKills() <= this.getTotalKills())
		{
			this.kdr = total_kills/total_deaths;
		}
		return kdr;
	}
	
	
	public void increaseKills()
	{
		this.total_kills += 1;

		FileManager.getInstance().getClanFile().set("Clans." + this.getName() + ".total-kills", total_kills);
		FileManager.getInstance().saveFiles();
	}
	
	public void increaseDeaths()
	{
		this.total_deaths += 1;

		FileManager.getInstance().getClanFile().set("Clans." + this.getName() + ".total-deaths", total_deaths);
		FileManager.getInstance().saveFiles();
	}
	
	public void setHQLocation(Location l)
	{
		this.clan_hq_loc = l;
		
		FileManager.getInstance().getClanFile().set("Clans." + this.getName() + ".hq-loc", Settings.get().serializeLoc(clan_hq_loc));
		FileManager.getInstance().saveFiles();
		
	}
	
	public void increaseLevel()
	{
		this.level += 1;
		this.xp_needed = ((this.xp_needed * 2) + 692);
		FileManager.getInstance().getClanFile().set("Clans." + this.getName() + ".level"  ,    level);
		FileManager.getInstance().getClanFile().set("Clans." + this.getName() + ".xp-next", xp_needed);
		FileManager.getInstance().saveFiles();
		
		this.broadcast(MessageManager.get().success("Your clan is now level " + this.getLevel() + "!"));
		this.broadcast(MessageManager.get().info("Current XP: " + this.getExp() + " | XP needed for next level: " + this.getExpNeeded()));
	}
	
	public void setLeader(Player p)
	{
		this.clan_leader = p.getUniqueId();
		FileManager.getInstance().getClanFile().set("Clans." + this.getName() + ".leader", this.getLeaderToString());
		FileManager.getInstance().saveFiles();
	}
	
	public void setCaptain(Player p)
	{
		if(this.getRecruits().contains(p.getUniqueId()))
		{
			this.getRecruits().remove(p.getUniqueId());
			FileManager.getInstance().getClanFile().set("Clans." + this.getName() + ".recruits", this.getRecruitsToString());
		}
		if(this.getSergeants().contains(p.getUniqueId()))
		{
			this.getSergeants().remove(p.getUniqueId());
			
			FileManager.getInstance().getClanFile().set("Clans." + this.getName() + ".sergeants", this.getSergeantsToString());
			
		}
		
		this.clan_captain = p.getUniqueId();
		FileManager.getInstance().getClanFile().set("Clans." + this.getName() + ".captain", this.getCaptainToString());
		FileManager.getInstance().saveFiles();
	}
	
	public void broadcast(String s)
	{
		for(UUID u : this.getMembers())
		{
			Bukkit.getPlayer(u).sendMessage(s);
		}
	}
	/**
	 * Increase the clan exp with a R range
	 * @param min the minimum integer to start the random from
	 * @param max the maximum integer that limits the random
	 */
	public void increaseExpRandomly(int min, int max)
	{
		Random rand = new Random();
		
		int r = rand.nextInt(max-min) + min;
		
		this.clan_xp += r;
		FileManager.getInstance().getClanFile().set("Clans." + this.getName() + ".xp", clan_xp);
		FileManager.getInstance().saveFiles();
		
		if(this.clan_xp >= xp_needed)
		{
			increaseLevel();
		}
	}
	
	public void increaseExp(int n)
	{
		this.clan_xp += n;
		
		FileManager.getInstance().getClanFile().set("Clans." + this.getName() + ".xp", clan_xp);
		FileManager.getInstance().saveFiles();
		
		if(this.clan_xp >= xp_needed)
		{
			increaseLevel();
		}
	}
	
	public void setExp(long n)
	{
		this.clan_xp = n;
	}
	
	public void setExpNeeded(long n)
	{
		this.xp_needed = n;
	}
	
	public void setLevel(int i)
	{
		this.level = i;
	}
	
	public void addMember(Player p)
	{
		this.getMembers().add(p.getUniqueId());
		this.getRecruits().add(p.getUniqueId());
		
		FileManager.getInstance().getClanFile().set("Clans." + this.getName() + ".members",  this.getMembersToString());
		FileManager.getInstance().getClanFile().set("Clans." + this.getName() + ".recruits",this.getRecruitsToString());
		
		FileManager.getInstance().saveFiles();
		
		this.broadcast(MessageManager.get().info(p.getName() + " is now part of " + this.getName() + "!"));
	}
	
	public void removeMember(Player p)
	{
		this.getMembers().remove(p.getUniqueId());
		
		if(this.getRecruits().contains(p.getUniqueId()))
		{
			this.getRecruits().remove(p.getUniqueId());
			FileManager.getInstance().getClanFile().set("Clans." + this.getName() + ".recruits", this.getRecruitsToString());
		}
		if(this.getSergeants().contains(p.getUniqueId()))
		{
			this.getSergeants().remove(p.getUniqueId());
			FileManager.getInstance().getClanFile().set("Clans." + this.getName() + ".sergeants", this.getSergeantsToString());
		}
		FileManager.getInstance().saveFiles();
		//
	}
	
}
