package me.yazan.luke.clansplus.clans;

import java.util.ArrayList;
import java.util.UUID;

import me.yazan.luke.clansplus.utils.FileManager;

public class ClanManager {
	
	public ArrayList<Clan> clans_list = new ArrayList<Clan>();
	
	private static ClanManager cm;
	
	public static ClanManager get()
	{
		return cm;
	}
	
	public Clan getClan(String name)
	{
		for(Clan c : clans_list)
		{
			if(c.getName().equalsIgnoreCase(name))
			{
				return c;
			}
		}
		return null;
	}
	
	public Clan createClan(UUID leader, String name, String tag)
	{
		//tag = s.substring(0, Math.min(tag.length(), 4));
		Clan c = new Clan(leader, name, tag);
		
		c.getMembers().add(leader);
		
		FileManager.getInstance().getClanFile().set("Clans." + c.getName() + ".leader",    c.getLeaderToString());
		FileManager.getInstance().getClanFile().set("Clans." + c.getName() + ".tag"   ,               c.getTag());

		FileManager.getInstance().getClanFile().set("Clans." + c.getName() + ".level",                         1);
		FileManager.getInstance().getClanFile().set("Clans." + c.getName() + ".total-kills",                   0);
		FileManager.getInstance().getClanFile().set("Clans." + c.getName() + ".total-deaths",                  1);
		
		FileManager.getInstance().getClanFile().set("Clans." + c.getName() + ".xp",                        1926L);
		FileManager.getInstance().getClanFile().set("Clans." + c.getName() + ".xp-next",                   5000L);

		FileManager.getInstance().getClanFile().set("Clans." + c.getName() + ".members",  c.getMembersToString());
		
		FileManager.getInstance().saveFiles();
		
		clans_list.add(c);
		return c;
	}
}
