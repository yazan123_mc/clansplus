package me.yazan.luke.clansplus.players;

import java.util.UUID;

import me.yazan.luke.clansplus.clans.Clan;
import me.yazan.luke.clansplus.clans.ClanManager;
import me.yazan.luke.clansplus.utils.FileManager;

import org.bukkit.entity.Player;

public class ClansPlayer {
	
	private Player    player;
	
	private String     title;
	private String last_seen;
	
	private int        kills,
	                  deaths;
	
	private float        kdr; //USE getKDR() DO NOT USE THIS
	
	public ClansPlayer(Player p)
	{
		this.player = p;
	}
	
	public boolean isInClan()
	{
		for(Clan c : ClanManager.get().clans_list)
		{
			if(c.getMembers().contains(player.getUniqueId())) 
				return true;
		}
		return false;
	}
	
	public Clan getClan()
	{
		for(Clan c : ClanManager.get().clans_list)
		{
			if(c.getMembers().contains(player.getUniqueId()))
			{
				return c;
			}
		}
		return null;
	}
	
	public UUID getUUID()
	{
		return player.getUniqueId();
	}
	
	public String getUUIDToString()
	{
		return player.getUniqueId().toString();
	}
	
	public int getKills()
	{
		return FileManager.getInstance().getPlayerFile().getInt("Players." + getUUIDToString() + ".kills");
	}
	
	public int getDeaths()
	{
		return FileManager.getInstance().getPlayerFile().getInt("Players." + getUUIDToString() + ".deaths");
	}
	
	public String getTitle()
	{
		return FileManager.getInstance().getPlayerFile().getString("Players." + getUUIDToString() + ".title");
	}
	
	public String getLastSeen()
	{
		return FileManager.getInstance().getPlayerFile().getString("Players." + getUUIDToString() + ".ls");
	}
	
	public float getKDR()
	{
		if(this.getKills() >= this.getDeaths())
		{
			this.kdr = deaths/kills;
			return kdr;
		}
		if(this.getKills() <= this.getDeaths())
		{
			this.kdr = kills/deaths;
		}
		return kdr;
	}
	
	public void setLastSeen(String s)
	{
		this.last_seen = s;
		
		FileManager.getInstance().getPlayerFile().set("Players." + getUUIDToString() + ".ls", last_seen);
		FileManager.getInstance().saveFiles();
		
	}
	public void incKills()
	{
		this.kills += 1;
		
		FileManager.getInstance().getPlayerFile().set("Players." + getUUIDToString() + ".kills", kills);
		FileManager.getInstance().saveFiles();
	}
	
	public void incDeaths()
	{
		this.deaths += 1;
		
		FileManager.getInstance().getPlayerFile().set("Players." + getUUIDToString() + ".deaths", deaths);
		FileManager.getInstance().saveFiles();
	}
	
	public void tryCreateNewProfile()
	{
		if(FileManager.getInstance().getPlayerFile().get("Players."+ getUUIDToString()) != null)
		{
			return;
		}
		
		this.kills  = 0;
		this.deaths = 0;
		
		FileManager.getInstance().getPlayerFile().set("Players." + getUUIDToString() + ".kills" ,  kills);
		FileManager.getInstance().getPlayerFile().set("Players." + getUUIDToString() + ".deaths", deaths);
		
		this.setTitle("Noob");
		this.setLastSeen("Unknown");
		FileManager.getInstance().saveFiles();
		
		
	}
	
	public void setTitle(String s)
	{
		this.title = s;
		
		FileManager.getInstance().getPlayerFile().set("Players." + getUUIDToString() + ".title" ,  title);
		FileManager.getInstance().saveFiles();
	}
}
