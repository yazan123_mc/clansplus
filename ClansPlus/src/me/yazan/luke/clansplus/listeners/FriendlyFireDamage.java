package me.yazan.luke.clansplus.listeners;

import me.yazan.luke.clansplus.players.ClansPlayer;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class FriendlyFireDamage implements Listener{
	
	@EventHandler
	public void onDmg(EntityDamageByEntityEvent e)
	{
		if(e.getDamager() instanceof Player &&
		   e.getEntity () instanceof Player)
		{
			Player p_damaged = (Player) e.getEntity ();
			Player p_damager = (Player) e.getDamager();
			
			ClansPlayer cp_damaged = new ClansPlayer(p_damaged);
			ClansPlayer cp_damager = new ClansPlayer(p_damager);
			
			if(cp_damager.getClan().equals(cp_damaged.getClan()))
			{
				e.setCancelled(true);
			}
		}
	}

}
