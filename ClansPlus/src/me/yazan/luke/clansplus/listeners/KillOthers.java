package me.yazan.luke.clansplus.listeners;

import me.yazan.luke.clansplus.players.ClansPlayer;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class KillOthers implements Listener{
	
	@EventHandler
	public void onDeath(PlayerDeathEvent e)
	{
		if(e.getEntity().getKiller() == null)
		{
			return;
		}
		
		Player died   =    e.getEntity();
		Player killer = died.getKiller();
		
		ClansPlayer dcp = new ClansPlayer(  died);
		ClansPlayer kcp = new ClansPlayer(killer);
		
		if(kcp.isInClan())
		{
			kcp.getClan().increaseKills();
			kcp.getClan().increaseExpRandomly(100, 200);
		}
		dcp.incDeaths();
		kcp.incKills ();
	}

}
