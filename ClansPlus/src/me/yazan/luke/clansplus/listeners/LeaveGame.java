package me.yazan.luke.clansplus.listeners;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import me.yazan.luke.clansplus.players.ClansPlayer;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class LeaveGame implements Listener
{
	
	@EventHandler
	public void onLeave(PlayerQuitEvent e)
	{
		DateFormat date_f = new SimpleDateFormat("dd/MM/yyyy");
		DateFormat time_f = new SimpleDateFormat("HH:mm");
		Date date = new Date();
		Date time = new Date();
		
		Player p = e.getPlayer();
		ClansPlayer cp = new ClansPlayer(p);
		
		cp.setLastSeen(date_f.format(date) + " at " + time_f.format(time));
	}

}
