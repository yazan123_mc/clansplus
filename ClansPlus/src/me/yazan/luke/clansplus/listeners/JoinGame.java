package me.yazan.luke.clansplus.listeners;

import me.yazan.luke.clansplus.players.ClansPlayer;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinGame implements Listener{
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e)
	{
		ClansPlayer pl = new ClansPlayer(e.getPlayer());
		
		pl.tryCreateNewProfile();
	}

}
